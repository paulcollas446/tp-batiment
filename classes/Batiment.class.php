<?php 

// classes / Batiment.class.php

/**
 * Classe Batiment
 * @author Paul Collas
 */
class Batiment {

    // LES CHAMPS

    private $adresse; // l’adresse du bâtiment
    private $nbPiecesMaxi; // le nombre maximum de pièces du bâtiment
    private $nbPieces; // le nombre de pièces du bâtiment
    private $tabPieces; // tableau contenant les pièces du bâtiment


    // LES METHODES 
    
    /**
     * Constructeur
     */
    public function __construct(string $adresse, int $nbPiecesMaxi, array $tabPieces)
    {
        try {
            $this
            ->setAdresse($adresse)
            ->setNbPiecesMaxi($nbPiecesMaxi)
            ->setTabPieces($tabPieces)
            ->setNbPieces();
        } catch (Exception $e) {
            echo 'Exception : ',  $e->getMessage(), "\n";
        }
    }

    /**
     * Permet de retourner le nombre de pièces
     * 
     * @return integer
     */
    public function NbPieces() : int
    {
        return $this->nbPieces;
    }

    /**
     * Permet de retourner le nombre maxi de pièces
     * 
     * @return integer
     */
    public function NbPiecesMaxi() : int
    {
        return $this->nbPiecesMaxi;
    }

    /**
     * Permet d’ajouter une pièce
     * 
     * @param Piece $piece
     * @return void
     */
    public function ajouterPiece(Piece $piece) 
    {
        if ($this->getNbPieces() < $this->getNbPiecesMaxi()) {
            $this->tabPieces[$piece->getNom()] = $piece;
            $this->setNbPieces();
        } else {
            throw new Exception("Le nombre de pièce est au maximum");
        }
    }

    /**
     * Donne le nombre de pièce que l’on peut encore ajouter
     * 
     * @return integer
     */
    public function getPieceLibre() : int
    {
        return ($this->getNbPiecesMaxi() - $this->getNbPieces());
    }

    /**
     * Affiche les informations pertinentes sur le bâtiment, les pièces, les meubles.
     * 
     * @return string
     */
    public function display() : string
    {
        $test = "Le bâtiment à l'adresse possédant {$this->getNbPieces()} pièce(s) dont : \n";
        foreach ($this->getTabPieces() as $pieces) {
            $test .= $pieces->display();
        }
        return $test;
    }

    /**
     * Affiche les test des différentes classes du Meubles
     *
     * @return string
     */
    public function __toString() 
    {        
        $test = "Vous avez instancié la classe Batiment avec comme parramètres : \n";
        $test .= "L'addresse : {$this->getAdresse()} \n";
        $test .= "Accueillant {$this->getNbPieces()} Pièce(s)\n";
        $test .= "Le nombre de pièce maximum est de {$this->getNbPiecesMaxi()}\n";
        $test .= "Il est possible d'ajouter encore {$this->getPieceLibre()} pièces.\n";
        
        return $test; 
    }
    // GETTER & SETTER

    /**
     * Getter de la valeur de l'adresse du batiment
     * 
     * @return string
     */
    public function getAdresse() : string
    {
        return $this->adresse;
    }

    /**
     * Setter de la valeur de l'adresse du batiment
     * 
     * @param string $adresse
     * @return self
     */
    public function setAdresse(string $adresse) 
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Getter de la valeur du nombre de pieces du batiment
     * 
     * @return integer
     */
    public function getNbPieces() : int
    {
        return $this->nbPieces;
    }

    /**
     * Setter de la valeur du nombre de pieces du batiment
     * 
     * @return self
     */
    public function setNbPieces()
    {
        $this->nbPieces = count($this->getTabPieces());

        return $this;
    }

    /**
     * Getter de la valeur du tableau des pieces du batiment
     * 
     * @return array
     */
    public function getTabPieces() : array
    {
        return $this->tabPieces;
    }

    /**
     * Setter de la valeur du tableau des pieces du batiment
     * 
     * @param array $tabPieces
     * @return self
     */
    public function setTabPieces(array $tabPieces)
    {
        foreach($tabPieces as $pieces) {
            if(gettype($pieces) === "object" && get_class($pieces) === "Piece") {
                $this->tabPieces[$pieces->getNom()] = $pieces;
            } else {
                throw new Exception('Ce n\'est pas une pièce');
            }
        }

        return $this;
    }

    /**
     * Getter du maximum de pièce du batiment
     * 
     * @return integer
     */
    final public function getNbPiecesMaxi() : int
    {
        return $this->nbPiecesMaxi;
    }

    /**
     * Setter du maximum de pièce du batiment
     * 
     * @param integer $nbPiecesMaxi
     * @return self
     */
    final public function setNbPiecesMaxi(int $nbPiecesMaxi)
    {
        $this->nbPiecesMaxi = $nbPiecesMaxi;

        return $this;
    }
}