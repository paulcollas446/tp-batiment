<?php 

// classes / Piece.class.php
/**
 * Classe Piece
 * @author Paul Collas
 */
class Piece extends Batiment{

    // LES CHAMPS

    private $largeur; // la largeur de la pièce
    private $hauteur; // la hauteur de la pièce
    private $profondeur; // la profondeur de la pièce
    private $nom; // le nom de la pièce ("S301", "Cuisine", etc)
    private $nbMeublesMaxi; // le nombre maximum de meubles que l’on peut mettre dans une pièce
    private $nbMeubles; // le nombre de meubles dans la pièce
    private $tabMeubles; // tableau contenant les meubles de la pièce

    // LES METHODES 

    /**
     * Constructeur
     * 
     * @param float $nbMeubles
     */
    public function __construct(int $largeur, int $hauteur, int $profondeur, string $nom, int $nbMeublesMaxi, array $tabMeubles )
    {
        try {
            $this
            ->setLargeur($largeur)
            ->setHauteur($hauteur)
            ->setProfondeur($profondeur)
            ->setNom($nom)
            ->setNbMeublesMaxi($nbMeublesMaxi)
            ->setTabMeuble($tabMeubles)
            ->setNbMeuble();

        } catch (Exception $e) {
            echo 'Pas de Pièces', $e->getMessage(), "\n";
        }
            
    }
    
    /**
     * Permet de retourner le nombre de meubles
     * 
     * @return float
     */
    public function NbMeuble() : float 
    {
        return $this->nbMeuble;
    }

    /**
     * Permet de retourner le nombre maximum de meubles
     */
    public function nbMeublesMaxi() : float
    {
        return $this->nbMeublesMaxi;
    }

    /**
     * Permet d’ajouter un meuble
     * 
     * @param Meuble $meuble
     * @return void
     */
    public function ajouterMeuble(Meuble $meuble)
    {
        if((round($this->surfaceLibre() - ($meuble->getSurface()/100))) > 0 && $this->getNbMeuble() < $this->getNbMeublesMaxi()) {
            $this->tabMeubles[$meuble->getNom()] = $meuble;
            $this->setNbMeuble(); //Mettre à jour le nombre de meuble
        } else {
            throw new Exception("Il y'a plus de place dans les salles pour ajouter ce meuble");
        }
    }

    /**
     * Permet de supprimer un meuble
     * 
     * @param string $meuble
     * @return void
     */
    public function suppMeuble(string $meuble)
    {
        unset($this->tabMeubles[$meuble]);
        $this->setNbMeuble(); //Mettre à jour le nombre de meuble 
    }

    /**
     * Donne la surface de la pièce
     * 
     * @return integer
     */
    public function getSurface() : int
    {
        return ($this->getLargeur() * $this->getProfondeur());
    }

    /**
     * Donne le volume de la pièce
     */
    public function getVolume() : int
    {
        return ($this->getSurface() * $this->getHauteur());
    }

    /**
     * Donne la surface prise par les meubles
     * 
     * @return integer
     */
    public function surfaceMeubles() : int
    {
        $tableau = $this->getTabMeuble();
        $surfaceTotal = 0;
        foreach ($tableau as $meubles) {
            $surfaceTotal = ($surfaceTotal + $meubles->getSurface());
        }

        return round($surfaceTotal/100); // La surface totale est remis en metres et arrondi
    }


    /**
     * Donne la surface libre
     */
    public function surfaceLibre() : int
    {
        return round(($this->getSurface() - $this->surfaceMeubles()));
    }


    /**
     * Affiche les informations pertinentes sur la pièce, les meubles
     * 
     * @return string
     */
    public function affiche()
    {
        $test = "{$this->getNom()}". "de {$this->getSurface()} metres carres " .  "composé de:\n";
        foreach ($this->getTabMeuble() as $furniture) {
            $test .= $furniture->display();
        }
        return $test;
    }


     /**
      * Affiche les test des différentes classes du Meubles
      * 
      * @return string
      */
      public function __toString()
      {

        $test = "Vous avez instancié la classe Pièce nommé '{$this->getNom()}' voici ses attributs : \n";
        $test .= "La largeur : {$this->getLargeur()} cm \n";
        $test .= "La hauteur : {$this->getHauteur()} cm \n";
        $test .= "La profondeur : {$this->getProfondeur()} cm \n";

        $test .= "Comportant {$this->getNbMeuble()} meubles \n";
        $test .= "Le meuble fait : {$this->getVolume()} m³ \n";
        $test .= "La surface du meuble est de : {$this->getSurface()} m² \n";
        $test .= "Les meubles utilisent au total {$this->surfaceMeubles()} m² \n";
        $test .= "Il reste donc {$this->surfaceLibre()} m² de libre. \n";
        $test .= "le nombre maximal de meuble est de {$this->getnbMeublesMaxi()} \n";
        
        return $test;
      }

    // GETTER & SETTER

    /**
     * Getter de la valeur de la largeur de la pièce
     * 
     * @return integer
     */
    public function getLargeur() : int
    {
        return $this->largeur;
    }

    /**
     * Setter de la valeur de largeur de la pièce
     * 
     * @param integer $largeur
     * @return self
     */
    public function setLargeur(int $largeur)
    {
        $this->largeur = $largeur;

        return $this;
    }

    /**
     * Getter de la valeur de la hauteur de la pièce
     * 
     * @return integer
     */
    public function getHauteur() : int
    {
        return $this->hauteur;
    }

    /**
     * Setter de la valeur de hauteur de la pièce
     * 
     * @param integer $hauteur
     * @return self
     */
    public function setHauteur(int $hauteur)
    {
        $this->hauteur = $hauteur;

        return $this;
    }

    /**
     * Getter de la valeur de la profondeur de la pièce
     * 
     * @return integer
     */
    public function getProfondeur() : int
    {
        return $this->profondeur;
    }

    /**
     * Setter de la valeur de profondeur de la pièce
     * 
     * @param integer $profondeur
     * @return self
     */
    public function setProfondeur(int $profondeur)
    {
        $this->profondeur = $profondeur;

        return $this;
    }

    /**
     * Getter the value of nom de la pièce
     * @return string
     */
    public function getNom() : string
    {
        return $this->nom;
    }

    /**
     * Setter the value of nom de la pièce
     *
     * @param string $nom
     * @return  self
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Getter de la valeur du nombre de meuble maxi de la pièce
     * 
     * @return integer
     */
    final private function getNbMeublesMaxi() : int
    {
        return $this->nbMeublesMaxi;
    }

    /**
     * Setter de la valeur du nombre de meuble maxi de la pièce
     * 
     * @param integer $nbMeublesMaxi
     * @return self
     */
    final private function setNbMeublesMaxi()
    {
        $this->nbMeublesMaxi = 8;

        return $this;
    }
    
    /**
     * Getter de la valeur du nombre de meubles
     * 
     * @return integer
     */
    public function getNbMeuble() : int 
    {
        return $this->nbMeubles;
    }

    /**
     * Setter la valeur du nombre de meuble
     * 
     * @return self
     */
    public function setNbMeuble()
    {
        $this->nbMeubles = count($this->getTabMeuble());

        return $this;
    }

    /**
     * Getter de la valeur du tableau des meubles
     * 
     * @return array
     */
    public function getTabMeuble() : array 
    {
        return $this->tabMeubles;
    }

    /**
     * Ajout d'un meuble dans le tableau du nombre des meubles
     * 
     * @param array $tabMeubles
     * @return self
     */
    public function setTabMeuble(array $tabMeubles) 
    {
        foreach ($tabMeubles as $meuble) {
            if(gettype($meuble) === "object" && get_class($meuble) === "Meuble") {
                $this->tabMeubles[$meuble->getNom()] = $meuble;
            } else {
                throw new Exception('L\'objet n\'est pas un meuble dans la pièce');
            }
        }

        return $this;
    }
}

