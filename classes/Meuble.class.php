<?php

// classes / Meuble.class.php

/**
 * Class Meuble
 * @author Paul Collas
 */
class Meuble extends Piece {

    // LES CHAMPS

    private $largeur = 0; // la largeur du meuble
    private $hauteur = 0; // la hauteur du meuble
    private $profondeur = 0; // la profondeur du meuble
    private $nom; // le nom du meuble

    // LES METHODES 

    /**
     * Constructeur
     */
    public function __construct(int $largeur, int $hauteur, int $profondeur, string $nom = "Meuble")
    {
        $this
        ->setLargeur($largeur)
        ->setHauteur($hauteur)
        ->setProfondeur($profondeur)
        ->setNom($nom);
    }

    /**
     * Calcul la surface maximale prise par le meuble
     * 
     * @return int
     */
    public function getSurface(): int 
    {
        return ($this->getLargeur() * $this->getProfondeur());
    }

    /**
     * Calcule le volume d’encombrement du meuble
     * 
     * @return int
     */
    public function getVolume() : int 
    {
        return ($this->getSurface() * $this->getHauteur());
    }

    /**
     * Affiche les différentes informations perninente sur le meuble
     * 
     * @return string
     */
    public function affiche() : string
    {
        return "1 {$this->getNom()} de {$this->getVolume()} cm³.\n";
    }

    /**
     * Affiche les test des différentes classes du Meubles
     * 
     * @return string
     */
    public function __toString()  
    {
        $test = "Votre meuble s'appelle " . $this->getNom() . "\n" . "Le volume de votre " . $this->getNom() . " est : " . $this->getVolume() . " metres cubes" . "\n" . "La surface de votre " . $this->getNom() . " est : " . $this->getSurface() . " metres carré" . "\n";
        
        return $test;
    }

    // GETTER & SETTER

    /**
     * Getter de la valeur de la largeur du meuble
     * 
     * @return integer
     */
    public function getLargeur() : int
    {
        return $this->largeur;
    }

    /**
     * Setter de la valeur de largeur du meuble
     * 
     * @param integer $largeur
     * @return self
     */
    public function setLargeur(int $largeur)
    {
        $this->largeur = $largeur;

        return $this;
    }

    /**
     * Getter de la valeur de la hauteur du meuble
     * 
     * @return integer
     */
    public function getHauteur() : int
    {
        return $this->hauteur;
    }

    /**
     * Setter de la valeur de hauteur du meuble
     * 
     * @param integer $hauteur
     * @return self
     */
    public function setHauteur(int $hauteur)
    {
        $this->hauteur = $hauteur;

        return $this;
    }

    /**
     * Getter de la valeur de la profondeur du meuble
     * 
     * @return integer
     */
    public function getProfondeur() : int
    {
        return $this->profondeur;
    }

    /**
     * Setter de la valeur de profondeur du meuble
     * 
     * @param integer $profondeur
     * @return self
     */
    public function setProfondeur(int $profondeur)
    {
        $this->profondeur = $profondeur;

        return $this;
    }

    /**
     * Get the value of nom du meuble
     * @return string
     */
    public function getNom() : string
    {
        return $this->nom;
    }

    /**
     * Set the value of nom du meuble
     *
     * @param string $nom
     * @return  self
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }
    
}