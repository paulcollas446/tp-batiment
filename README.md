## Sommaire
1. [Infos Générales](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Collaboration](#collaboration)
5. [FAQs](#faqs)
## Informations générales
***
Crétion d'une application PHP. On souhaite représenter un bâtiment sous une forme objet. On considère qu’un
bâtiment est constitué de pièces et que dans chacune des pièces se trouvent des
meubles.
### La liste des classes 
Tout les fichiers de classes se situe dans le dossier ```classes``` :

La classe **Meuble** possède les caractéristiques suivantes :

**Les champs :**
* largeur de type « int »
* hauteur de type « int »
* profondeur de type « int »
* nom de type « String » contient le nom du meuble

**Les méthodes :**
* volume : retourne un entier de type « long » et calcule le volume d’encombrement du
meuble,
* surface : retourne un entier « long » qui donne la surface maximale prise par le
meuble,
* affiche : affiche des informations pertinentes sur le meuble au choix.

La classe **Piece** possède les caractéristiques suivantes :

**Les champs :**
* largeur de type « int »,
* hauteur de type « int »,
* profondeur de type « int »,
* nom de type « String » contenant le nom de la pièce ("S301", "Cuisine", etc),
* nbMeublesMaxi qui contient le nombre maximum de meubles que l’on peut mettre
dans une pièce. Il est de type « final private int »,
* nbMeubles de type « private int » qui contient le nombre de meubles dans la pièce,
* tabMeubles qui est un tableau contenant les meubles de la pièce. Il est privé.

**Les méthodes :**
* définir une méthode « public » permettant de retourner le nombre de meubles,
* définir une méthode « public » permettant de retourner le nombre maximum de
meubles,
* définir une méthode « public » permettant d’ajouter un meuble,
* volume : « private », retourne un entier « long » qui donne le volume de la pièce,
* surface : « private », retourne un entier « long » qui donne la surface de la pièce,
* surfaceMeubles : « public », retourne un entier « long » qui donne la surface prise par
les meubles,
* surfaceLibre : « public », retourne un entier « long » qui donne la surface libre,
* affiche : affiche les informations pertinentes sur la pièce, les meubles.

La classe **Batiment** possède les caractéristiques suivantes :

**Les champs :**
* adresse de type « String » qui contient l’adresse du bâtiment,
* nbPiècesMaxi de type « final private int » qui contient le nombre maximum de pièces
du bâtiment,
* nbPieces de type « private int » qui contient le nombre de pièces du bâtiment,
* tabPieces de type « private » qui est un tableau contenant les pièces du bâtiment,

**Les méthodes :**
* définir une méthode « public » permettant de retourner le nombre de pièces,
* définir une méthode « public » permettant de retourner le nombre maxi de pièces,
* définir une méthode « public » permettant d’ajouter une pièce,
* piecesLibres : « public », retourne un entier « int » qui donne le nombre de pièce que
l’on peut encore ajouter,
* affiche : « public », affiche les informations pertinentes sur le bâtiment, les pièces, les
meubles.
### Dictionnaire des données
![Image text](screenshot/Dictionnaire-données.PNG)
### Règles de gestion
R1. **UN OU PLUSIEURS** meuble(s) est possedé par une **UNE** piece.

R2. **UNE** piece possède **UN OU PLUSIEURS** meuble(s).

R3. **UNE OU PLUSIEURS** pieces est comporté dans **UN** batiment.

R4. **UN** batiment comporte **UNE OU PLUSIEURS** pièces.

### MCD 
![Image text](screenshot/mcd.PNG)

## Technologies
***
La liste des technologies utilisés:
* [PHP](https://www.php.net/docs.php): Version : 7.4.12 (cli)
## Installation du projet
***
Le projet PHP ne contient aucunes dépendances :
```
$ git clone https://gitlab.com/paulcollas446/tp-batiment.git
$ cd tp-batiment
```
## Lancement du projet
```
$ php .\batiment.php
```
PS: Les classes du projet se situe dans le document ```classes``` et la principale se situe ```à la racine``` du projet
## Collaboration
***
Le projet TP-BATIMENT est un projet pédagogique pour une évaluation du Language Object lors de ma formation au Bachelor Développeur Full Stack à MyDigitalSchhol à Caen.
> Paul COLLAS est le seul contributeur
