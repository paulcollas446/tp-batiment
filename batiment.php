<?php // batiment.php
spl_autoload_register(function($classe){
    include "classes/" . $classe . ".class.php";
});

/***************** MEUBLE ******************/
echo "Bonjour Vincent,\n";

echo "\n";
echo "\n";

// Test d'un meuble : armoire
$armoire = new Meuble(1, 3, 2, "Armoire");
echo $armoire;

echo "--------------------------------------- \n";
echo "--------------------------------------- \n";

// Test d'un autre meuble : bureau
$bureau = new Meuble(2, 4, 4, "bureau");
echo $bureau;

echo "--------------------------------------- \n";
echo "--------------------------------------- \n";

// Test d'un autre meuble : lit
$lit = new Meuble(3, 3, 3, "lit");
echo $lit;

echo "--------------------------------------- \n";
echo "--------------------------------------- \n";

// Test d'un autre meuble : TV
$tv = new Meuble(1, 1, 1, "tv");
echo $tv;

echo "--------------------------------------- \n";
echo "--------------------------------------- \n";

// Test d'un autre meuble : Etagère 
$etagere = new Meuble(6, 10, 2, "etagere");
echo $etagere;

echo "--------------------------------------- \n";
echo "--------------------------------------- \n";

echo " \n";
echo " \n";
/***************** PIECE ******************/

$cuisine =  new Piece(30, 3, 30, "Cuisine", 5, [$etagere]);
echo $cuisine;

echo "--------------------------------------- \n";
echo "--------------------------------------- \n";

$chambre =  new Piece(30, 3, 30, "chambre", 4, [$lit, $bureau]);
$chambre->ajouterMeuble($tv);
echo $chambre;

echo "--------------------------------------- \n";
echo "--------------------------------------- \n";

echo " \n";
echo " \n";
/***************** BATIMENT ******************/

$immeuble = new Batiment("13 Rue Prévert - Caen", 50, [$cuisine], 35 );
try {
    $immeuble->ajouterPiece($chambre);
} catch (Exception $e) {
    echo 'Exception received : ',  $e->getMessage(), "\n";
}
echo $immeuble;

echo " \n";
echo " \n";


/***************** TEST TOUTES LES CLASSES ******************/

// $index = new Index();
// echo $index->index();